import JsonContent from '../json-content';
import React from 'react';
import Btn from '../component-templates/button';
import Txt from '../component-templates/typography';
import { Typography } from '@material-ui/core';


const componentMap: { [index: string]: React.FC } = {
    'button': Btn,
    'text': Txt,
};

const RenderComponent: React.FC<JsonContent> = ({name, props}) => {
    let component: React.FC = componentMap[name];
    if (!component) {return (<Typography>Component {name} not found!</Typography>)}
    return React.createElement(component, props);
}

export default RenderComponent;