import { Button } from '@material-ui/core';
import React from 'react';

const Btn: React.FC = ({children, ...props}) => <Button {...props}>{children}</Button>;

export default Btn;